﻿using UnityEngine;
using System.Collections;

public class GUIJamarat : VRGUI {
	int ula, wusta, kubra;
	bool flagUla, flagWusta, flagKubra;
	string sceneName;
	GameObject back;
	// Use this for initialization
	void Start () {
		sceneName = Application.loadedLevelName;
		flagUla = true;
		flagWusta = false;
		flagKubra = false;
		if (sceneName == "jamarat_simulation_phase1") {
			flagKubra = true;
			flagUla = false;
			flagWusta = false;
		}
	}
	
	// Update is called once per frame
	void Update () {
		ula = JamaratBehavior.ula;
		wusta = JamaratBehavior.wusta;
		kubra = JamaratBehavior.qubra;
		if (ula == 7) {
			flagUla = false;
			flagWusta = true;
		} else if (wusta == 7) {
			flagWusta = false;
			flagKubra = true;
		}
	}
	
	public override void OnVRGUI()
	{
		GUILayout.BeginArea(new Rect(80f, 0f, (Screen.width), Screen.height));
		if (flagUla) GUILayout.Button ("lemparan batu ke jamarat Ula: " + JamaratBehavior.ula);
		if (flagWusta) GUILayout.Button ("lemparan batu ke jamarat Wusta: " + JamaratBehavior.wusta);
		if (flagKubra) GUILayout.Button ("lemparan batu ke jamarat Kubra: " + JamaratBehavior.qubra);
		GUILayout.EndArea ();
	}
}
