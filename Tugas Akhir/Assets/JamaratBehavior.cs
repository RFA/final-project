﻿using UnityEngine;
using System.Collections;

public class JamaratBehavior : MonoBehaviour {
	public static int ula, wusta, qubra;
	string sceneName;
	GameObject kembali;
	// Use this for initialization
	void Start () {
		ula = wusta = qubra = 0;
		kembali = GameObject.FindGameObjectWithTag ("BackButton");
		sceneName = Application.loadedLevelName;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnCollisionEnter(){
		string tag = this.tag;
		if (sceneName == "jamarat_simulation_phase2"){
			switch (tag) {
			case "Ula":
				ula++;
				break;
			case "Wusta":
				if (ula >= 7)
				{
					wusta++;
				}
				break;
			case "Qubra":
				if (wusta >=7)
				{
					qubra++;
				}
				if (qubra ==7)
				{
					kembali.renderer.enabled = true;
				}
				break;
			default:
				break;
			}
		}
		else if (sceneName == "jamarat_simulation_phase1")
		{
			switch (tag) {
			case "Qubra":
				qubra++;
				if (qubra >=7)
				{
					kembali.renderer.enabled = true;
				}
				break;
			default:
				break;
			}
		}
	}
}
