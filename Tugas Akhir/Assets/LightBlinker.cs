﻿using UnityEngine;
using System.Collections;

public class LightBlinker : MonoBehaviour {
	int count;
	public int maxCount = 50;
	public int mod = 100;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (count++ > maxCount) {
			this.light.intensity = 0;
		} else {
			this.light.intensity = 2;
		}
		count %= mod;
	}
}
