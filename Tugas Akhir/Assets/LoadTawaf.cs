﻿using UnityEngine;
using System.Collections;

public class LoadTawaf : MonoBehaviour {
	string objectName;
	GameObject player;
	Vector3 pos = new Vector3(-3.5f, 12.0f, -3.5f);
	Vector3 encyclopedia = new Vector3 (-23.5f, 0f, 0f);
	Vector3 simulation = new Vector3 (24.5f, 0f, 0f);
	Vector3 firstPos = new Vector3 (0f, 0f, 0f);
	// Use this for initialization
	void Start () {
		player = GameObject.FindGameObjectWithTag ("Player");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter(Collider Other){
		objectName = this.gameObject.name;
		if (Other.collider.CompareTag("Player")) {
			Debug.Log(objectName);
			switch (objectName){
			case "KembaliLevel":
				if (this.renderer.enabled == true)
					Application.LoadLevel(0);
				break;
			case "Masjidil":
				Application.LoadLevel(1);
				break;
			case "Arafah":
				Application.LoadLevel(2);
				break;
			case "Jamarat":
				Application.LoadLevel(3);
				break;
			case "Mina":
				Application.LoadLevel(4);
				break;
			case "Tawaf":
				Application.LoadLevel(5);
				break;
			case "Sai":
				Application.LoadLevel(6);
				break;
			case "ArafahWukuf":
				Application.LoadLevel(7);
				break;
			case "Jumrah1":
				Application.LoadLevel(8);
				break;
			case "Jumrah2":
				Application.LoadLevel(9);
				break;
			
			
			case "SimulationEvent":
				player.transform.position = simulation;
				break;
			case "EncyclopediaEvent":
				player.transform.position = encyclopedia;
				break;
			case "Kembali":
				player.transform.position = firstPos;
				break;
			default:
				break;
			}
		}
	}
}
