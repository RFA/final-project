﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
public class MainMenuScript : MonoBehaviour {
	public static GameObject[] signs;
	public bool[] questions;
	//public int z;
	[SerializeField]
	private GameObject rocks;
	// Use this for initialization
	void Start () {
		questions = new bool[10];
		for (int i=0 ; i<10 ; i++){
			questions[i] = false;
		}
		//z = 1500;
		//rocks = GameObject.FindGameObjectWithTag ("item");
		//rocksRigidbody = rocks.GetComponent<Rigidbody> ();
		//rocksRigidbody.WakeUp ();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.V)) {
			GameObject go = (GameObject)Instantiate(rocks, this.transform.position, this.transform.rotation);
			Vector3 temp = new Vector3(0,0,1500);
			temp = this.transform.FindChild("OVRCameraRig").transform.rotation * temp;
			temp.y = 0;
			// Debug.Log("throw rock here: " + temp);

			//rocksRigidbody.AddForce(0,0,rocks.transform.forward * Time.deltaTime);
			Rigidbody rocksRigidbody = go.GetComponent<Rigidbody>();
			rocksRigidbody.velocity = Vector3.zero;
			rocksRigidbody.angularVelocity = Vector3.zero;
			rocksRigidbody.AddForce(temp);
		}
	}
	void clearQuestions(){
		for (int i=0; i<10; i++) {
			questions[i] = false;
		}
	}
	void OnTriggerEnter(Collider other){
		if (other.gameObject.CompareTag ("Signs")) {
			if (other.gameObject.name == "shield_tawaf")
			{
				clearQuestions();
				questions[0] = true;
			}
		}
	}
	/*public override void OnVRGUI (){
		while (questions[0]) {
			if (GUILayout.Button ("Apakah anda ingin menuju level tawwaf?: "));
			if (GUILayout.Button ("Ya"))
				Application.LoadLevel ("scene11_rifi_tawaf");
			if (GUILayout.Button ("Tidak"))
				questions[0] = false;
		}
	}*/
}

