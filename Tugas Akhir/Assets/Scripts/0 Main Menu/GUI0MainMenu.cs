﻿using UnityEngine;
using System.Collections;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
public class GameControl : MonoBehaviour {
	public static GameControl control;
	public bool main;
	public bool one = false;
	public bool two = false;
	public bool three = false;
	public bool four = false;
	public bool five = false;
	public bool six = false;
	public bool seven = false;
	public bool eight = false;
	public bool nine = false;
	public bool ten = false;
	public bool eleven = false;
	public bool twelve = false;
	public bool thirteen = false;
	public bool fourteen = false;
	public bool fifteen = false;
	public bool sixteen = false;
	public bool seventeen = false;
	public int score;
	// Use this for initialization
	void Awake () {
		if (control == null) 
		{
			DontDestroyOnLoad (gameObject);
			control = this;
		} 
		else if (control != this) 
		{
			Destroy(gameObject);
		}
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Save()
	{
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Create (Application.persistentDataPath + "/playerInfo.dat");
		PlayerData player = new PlayerData ();
		player.main = main;
		player.one = one;
		player.two = two;
		player.three = three;
		player.four = four;
		player.five = five;
		player.six = six;
		player.seven = seven;
		player.eight = eight;
		player.nine = nine;
		player.ten = ten;
		player.eleven = eleven;
		player.twelve = twelve;
		player.thirteen = thirteen;
		player.fourteen = fourteen;
		player.fifteen = fifteen;
		player.sixteen = sixteen;
		player.seventeen = seventeen;
		player.score = score;
		bf.Serialize (file, player);
		file.Close ();
	}

	public void Load()
	{
		if (File.Exists (Application.persistentDataPath + "/playerInfo.dat")) 
		{
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Open (Application.persistentDataPath + "/playerInfo.dat",FileMode.Open);
			PlayerData player = (PlayerData)bf.Deserialize(file);
			file.Close();
			main = player.main;
			one = player.one;
			two = player.two;
			three = player.three;
			four = player.four;
			five = player.five;
			six = player.six;
			seven = player.seven;
			eight = player.eight;
			nine = player.nine;
			ten = player.ten;
			eleven = player.eleven;
			twelve = player.twelve;
			thirteen = player.thirteen;
			fourteen = player.fourteen;
			fifteen = player.fifteen;
			sixteen = player.sixteen;
			seventeen = player.seventeen;
			score = player.score;
		}
	}
}

[Serializable]
class PlayerData
{
	public bool main;
	public bool one;
	public bool two;
	public bool three;
	public bool four;
	public bool five;
	public bool six;
	public bool seven;
	public bool eight;
	public bool nine;
	public bool ten;
	public bool eleven;
	public bool twelve;
	public bool thirteen;
	public bool fourteen;
	public bool fifteen;
	public bool sixteen;
	public bool seventeen;
	public int score;
}
