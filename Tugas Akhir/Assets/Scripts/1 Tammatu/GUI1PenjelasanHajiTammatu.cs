﻿using UnityEngine;
using System.Collections;

public class GUI1PenjelasanHajiTammatu : VRGUI {
	public Texture tex;
	public bool flag = false;
	string penjelasan;
	string name;
	// Use this for initialization
	public override void OnVRGUI()
	{
		if (flag == true)
		{
			switch(name){
			case "inf_maqam":
				GUILayout.BeginArea(new Rect(0f, 0f, Screen.width, Screen.height));
				GUILayout.Button("Maqam Ibrahim");
				GUILayout.Button("Prasati di depan Ka'bah yang berbentuk kotak dengan pahatan\n yang mengikuti jejak kaki Nabi Ibrahim saat membangun baitullah");
				GUILayout.EndArea();
				break;
			case "inf_kabah":
				GUILayout.BeginArea(new Rect(0f, 0f, Screen.width, Screen.height));
				GUILayout.Button("Kabah");
				GUILayout.Button("Bangunan sebagai arah ibadah sholat umat Islam" );
				GUILayout.EndArea();
				break;
			case "tamattu":
				GUILayout.BeginArea(new Rect(0f, 0f, Screen.width, Screen.height));
				GUILayout.Button("Haji Tamattu");
				GUILayout.Button("Haji Tamattu artinya adalah melaksanakan Ibadah Umrah terlebih dahulu" );
				GUILayout.Button("setelah itu baru melakukan Ibadah Haji" );
				GUILayout.EndArea();
				break;
			case "umrah":
				GUILayout.BeginArea(new Rect(0f, 0f, Screen.width, Screen.height));
				GUILayout.Button("Tahap Umrah (1/2):");
				GUILayout.Button("1. Melakukan Kiat haji mabrur" );
				GUILayout.Button("2. Bersuci" );
				GUILayout.Button("3. Memakai ihram di miqat" );
				GUILayout.Button("4. Membaca niat, talbiyah, sholawat dan doa sampai memasuki masjidil haram" );
				GUILayout.EndArea();
				break;
			case "umrah2":
				GUILayout.BeginArea(new Rect(0f, 0f, Screen.width, Screen.height));
				GUILayout.Button("Tahap Umrah (2/2):");
				GUILayout.Button("5. Melakukan thawaf" );
				GUILayout.Button("6. Sa'i" );
				GUILayout.Button("7. Tahalul" );
				GUILayout.EndArea();
				break;
			case "tawaf":
				GUILayout.BeginArea(new Rect(0f, 0f, Screen.width, Screen.height));
				GUILayout.Button("Tawaf (طواف / thawaf) adalah suatu ritual mengelilingi Ka'bah (bangunan suci di Mekkah)" );
				GUILayout.Button("sebanyak tujuh kali sebagai bagian pelaksanaan ibadah haji atau umrah." );
				GUILayout.EndArea();
				break;
			case "sai":
				GUILayout.BeginArea(new Rect(0f, 0f, Screen.width, Screen.height));
				GUILayout.Button("Ibadah Sa'i merupakan salah satu rukun umrah yang dilakukan dengan berjalan kaki (berlari-lari kecil)" );
				GUILayout.Button("bolak-balik 7 kali dari Bukit Shafa ke Bukit Marwah dan sebaliknya" );
				GUILayout.EndArea();
				break;
			case "infoBox1":
				GUILayout.BeginArea(new Rect(0f, 0f, Screen.width, Screen.height));
				GUILayout.Button("sentuh kotak di samping saya untuk memilih level" );
				GUILayout.EndArea();
				break;
			case "infoUla":
				GUILayout.BeginArea(new Rect(0f, 0f, Screen.width, Screen.height));
				GUILayout.Button("jamarat al-ula:" );
				GUILayout.Button("Jamarat pertama bernama jamarat al-ula" );
				GUILayout.Button("jamarat al-ula adalah jamrah terkecil" );
				GUILayout.EndArea();
				break;
			case "infoWusta":
				GUILayout.BeginArea(new Rect(0f, 0f, Screen.width, Screen.height));
				GUILayout.Button("jamarat al-wusta:" );
				GUILayout.Button("Jamarat kedua bernama jamarat al-wusta" );
				GUILayout.EndArea();
				break;
			case "infoKubra":
				GUILayout.BeginArea(new Rect(0f, 0f, Screen.width, Screen.height));
				GUILayout.Button("jamarat al-kubra:" );
				GUILayout.Button("Jamarat ketiga bernama jamarat al-kubra atau bisa juga disebut aqaba" );
				GUILayout.Button("Jamarat al-kubra merupakan jamarat terbesar" );
				GUILayout.EndArea();
				break;
			case "infoJamarat":
				GUILayout.BeginArea(new Rect(0f, 0f, Screen.width, Screen.height));
				GUILayout.Button("jamarat:" );
				GUILayout.Button("pada ibadah haji tamattu, jamarat dikunjungi sebanyak 2 kali" );
				GUILayout.Button("Yang pertama adalah ketika kembali ke mina. pada kunjungan pertama ini" );
				GUILayout.Button("melempar jumrah hanya dilakukan di jamarat al-kubra (aqaba)" );
				GUILayout.Button("Yang kedua adalah pada tanggal 11 - 13 dzulhijjah" );
				GUILayout.Button("pada kedatangan yang kedua ini, melempar jumrah dilakukan di" );
				GUILayout.Button("jamarat al-ula, al-wusta, dan al-kubra secara berurutan setiap harinya" );
				GUILayout.EndArea();
				break;
			case "infoTawaf":
				GUILayout.BeginArea(new Rect(0f, 0f, Screen.width, Screen.height));
				GUILayout.Button("Tawaf:" );
				GUILayout.Button("Tawaf (طواف / thawaf) adalah suatu ritual mengelilingi Ka'bah (bangunan suci di Mekkah) sebanyak 7 kali (minimal)." );
				GUILayout.Button("sebagai bagian pelaksanaan ibadah haji atau umrah" );
				GUILayout.Button("Perputaran tawaf dilakukan dengan cara mengelilingi ka'bah ke arah melawan jarum jam ");
				GUILayout.EndArea();
				break;
			case "infoSai":
				GUILayout.BeginArea(new Rect(0f, 0f, Screen.width, Screen.height));
				GUILayout.Button("Sa'i:" );
				GUILayout.Button("Ibadah Sa'i merupakan salah satu rukun umrah yang dilakukan dengan berjalan kaki (berlari-lari kecil) bolak-balik 7 kali" );
				GUILayout.Button("dari Bukit Shafa ke Bukit Marwah dan sebaliknya" );
				GUILayout.Button("Perputaran sai dilakukan dengan cara mengelilingi ka'bah ke arah melawan jarum jam, dimulai dari lajur yang paling kanan");
				GUILayout.EndArea();
				break;
			case "infoArafahDakwah":
				GUILayout.BeginArea(new Rect(0f, 0f, Screen.width, Screen.height));
				GUILayout.Button("Arafah:" );
				GUILayout.Button("Ibadah haji di arafah dilakukan dengan 2 rantai tahapan:" );
				GUILayout.Button("1. Mendengarkan dakwah di tenda masing-masing" );
				GUILayout.Button("2. Berdoa sampai waktu memasuki maghrib di sekitar lingkungan padang arafah");
				GUILayout.EndArea();
				break;
			case "infoArafahDoa":
				GUILayout.BeginArea(new Rect(0f, 0f, Screen.width, Screen.height));
				GUILayout.Button("Arafah:" );
				GUILayout.Button("Tempat ini adalah tempat di mana biasanya para jemaah haji berdo'a hingga waktu maghrib" );
				GUILayout.EndArea();
				break;
			case "infoJamaratPhase1":
				GUILayout.BeginArea(new Rect(0f, 0f, Screen.width, Screen.height));
				GUILayout.Button("Jamarat:" );
				GUILayout.Button("Pada jamarat fase pertama, lempar jumrah dilakukan hanya di aqaba saja" );
				GUILayout.EndArea();
				break;
			case "infoJamaratPhase2":
				GUILayout.BeginArea(new Rect(0f, 0f, Screen.width, Screen.height));
				GUILayout.Button("Jamarat:" );
				GUILayout.Button("Pada jamarat fase kedua, lempar jumrah dilakukan di jamarat al ula, wusta, kubra secara berurutan" );
				GUILayout.EndArea();
				break;
			case "infoMina":
				GUILayout.BeginArea(new Rect(0f, 0f, Screen.width, Screen.height));
				GUILayout.Button("Tenda mina:" );
				GUILayout.Button("Tenda mina adalah tempat para jemaah haji menginap dari tanggal 8 dzulhijah malam - 12 dzulhijah malam" );
				GUILayout.EndArea();
				break;
			default:
				break;
			}

			
		}
		else 
		{
			flag = false;
		}
	}
	void Start () {
	}

	public void OnTriggerEnter(Collider coll)
	{
		name = coll.gameObject.name;
		if (coll.gameObject.name == "inf_maqam")
		{
			flag = true;
		}
		if (coll.gameObject.name == "inf_kabah") 
		{
			flag = true;
		}
		if (coll.gameObject.name == "tamattu") 
		{
			flag = true;
		}
		if (coll.gameObject.name == "umrah") 
		{
			flag = true;
		}
		if (coll.gameObject.name == "umrah2") 
		{
			flag = true;
		}
		if (coll.gameObject.name == "tawaf") 
		{
			flag = true;
		}
		if (coll.gameObject.name == "sai") 
		{
			flag = true;
		}
		if (coll.gameObject.name == "infoBox1") 
		{
			flag = true;
		}
		if (coll.gameObject.name == "infoJamarat") 
		{
			flag = true;
		}
		if (coll.gameObject.name == "infoUla") 
		{
			flag = true;
		}
		if (coll.gameObject.name == "infoWusta") 
		{
			flag = true;
		}
		if (coll.gameObject.name == "infoKubra") 
		{
			flag = true;
		}
		if (coll.gameObject.name == "infoTawaf") 
		{
			flag = true;
		}
		if (coll.gameObject.name == "infoSai") 
		{
			flag = true;
		}
		if (coll.gameObject.name == "infoArafahDakwah") 
		{
			flag = true;
		}
		if (coll.gameObject.name == "infoArafahDoa") 
		{
			flag = true;
		}
		if (coll.gameObject.name == "infoJamaratPhase1") 
		{
			flag = true;
		}
		if (coll.gameObject.name == "infoJamaratPhase2") 
		{
			flag = true;
		}
		if (coll.gameObject.name == "infoMina") 
		{
			flag = true;
		}
	}

	public void OnTriggerExit(Collider coll)
	{
		flag = false;
	}
	// Update is called once per frame
	void Update () {
	
	}
	
}
