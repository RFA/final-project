﻿using UnityEngine;
using System.Collections;

public class GUI11Sai : VRGUI {
	GameObject back;
	// Use this for initialization
	void Start () {
		back = GameObject.FindGameObjectWithTag ("BackButton");
	}
	
	// Update is called once per frame
	void Update () {
		if (Laps.currentLap == 7)
			back.renderer.enabled = true;
	}

	public override void OnVRGUI ()
	{
		GUILayout.BeginArea(new Rect(80f, 0f, (Screen.width), Screen.height));
		if(GUILayout.Button("Current Checkpoint: " + Laps.currentCheckpoint));
		if(GUILayout.Button("Sekarang putaran ke: " + Laps.currentLap));
		GUILayout.EndArea ();
	}
}
