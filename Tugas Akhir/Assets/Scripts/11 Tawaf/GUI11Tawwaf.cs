﻿using UnityEngine;
using System.Collections;

public class GUI11Tawwaf : VRGUI {
	GameObject back;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (Laps.currentLap == 7) {
			back = GameObject.FindGameObjectWithTag("BackButton");
			back.renderer.enabled = true;
		}
	}

	public override void OnVRGUI()
	{
		GUILayout.BeginArea(new Rect(80f, 0f, (Screen.width), Screen.height));
		if(GUILayout.Button("Sekarang putaran ke: " + Laps.currentLap));
		GUILayout.EndArea ();
	}
}
