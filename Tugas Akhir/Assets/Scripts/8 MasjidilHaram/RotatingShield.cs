﻿using UnityEngine;
using System.Collections;

public class RotatingShield : MonoBehaviour {
	public BoxCollider bc;
	public float size = 0.005f;
	public float center = 0.004f;
	//public float smooth = 2.0F;
	public float tiltAngle = 30.0F;
	// Use this for initialization
	void Start () {

		if (MainMenuScript.signs == null || MainMenuScript.signs.Length == 0)
		{
			MainMenuScript.signs = GameObject.FindGameObjectsWithTag("Signs");
			foreach(GameObject sign in MainMenuScript.signs){
				bc = (BoxCollider) sign.gameObject.AddComponent(typeof(BoxCollider));
				bc.size = new Vector3(size,size,size);
				bc.center = new Vector3(0, center, 0);
				bc.isTrigger = true;
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void FixedUpdate(){
		//Vector3 pos = new Vector3 (this.transform.position.x + 5, this.transform.position.y + 4f, this.transform.position.z);
		//this.transform.Rotate(this.gameObject.transform.position * Time.deltaTime/3);
		float tiltAroundZ = Input.GetAxis("Horizontal") * tiltAngle;
		float tiltAroundX = Input.GetAxis("Vertical") * tiltAngle;
		Quaternion target = Quaternion.Euler(tiltAroundX, 0, tiltAroundZ);
		transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime * 10f);
	}
}
