﻿using UnityEngine;
using System.Collections;

public class Lifetime : MonoBehaviour 
{
	public float time = 10;
	private Rigidbody rigid;

	// Use this for initialization
	void Start () 
	{
		rigid = this.GetComponent<Rigidbody> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (rigid.IsSleeping())
		{
			Destroy(this.gameObject);
		}
	}
}
