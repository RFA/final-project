﻿using UnityEngine;
using System.Collections;

public class information : VRGUI {

	public bool flag = false;


	public override void OnVRGUI()
	{
		if (flag == true)
		{
			if(gameObject.name == "inf_maqam")
			{
				GUILayout.BeginArea(new Rect(0f, 0f, Screen.width, Screen.height));
				//GUILayout.FlexibleSpace();
				GUILayout.Button("Maqam Ibrahim");
				GUILayout.Button("Prasati di depan Ka'bah yang berbentuk kotak dengan pahatan\n yang mengikuti jejak kaki Nabi Ibrahim saat membangun baitullah");
				//GUILayout.FlexibleSpace();
				GUILayout.EndArea();
			}
			else if(gameObject.name == "inf_kabah")
			{
				GUILayout.BeginArea(new Rect(0f, 0f, Screen.width, Screen.height));
				//GUILayout.FlexibleSpace();
				GUILayout.Button("Kabah");
				GUILayout.Button("Bangunan sebagai arah ibadah sholat umat Islam" );
				//GUILayout.FlexibleSpace();
				GUILayout.EndArea();
			}


		}
		else 
		{
			flag = false;
		}
	}
	
	public void OnTriggerEnter(Collider coll)
	{
		if (coll.gameObject.name == "inf_maqam")
		{
			flag = true;
			gameObject.name = "inf_maqam";
			 
		}

		if (coll.gameObject.name == "inf_kabah") 
		{
			flag = true;
			gameObject.name = "inf_kabah";
		}
	}
	
	public void OnTriggerExit(Collider coll)
	{
		flag = false;
	}
}
